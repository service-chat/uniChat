import Vue from 'vue'
import Vuex from 'vuex'

//仿数据库数据
const userList = require("../../../server/store/userList.json");
const fastReply = require("../../../server/store/fastReply.json");
const productList = require("../../../server/store/product.json");
const messageList = require("../../../server/store/messageList.json")
const robotReply = require("../../../server/store/robotReply.json")
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    userList: userList,
    fastReply: fastReply,
    productList: productList,
    messageList: messageList,
    robotReply: robotReply
  },
  mutations: {},
  actions: {},
  modules: {}
})