# 参数说明文件

## data 数据

| 字段                  | 类型      | 描述                                             |
| --------------------- | --------- | ------------------------------------------------ |
| socket                | Object    | socket 对象                                      |
| sender                | Object    | 发送者对象                                       |
| revicer               | Object    | 接收者对象                                       |
| infoTemplate          | Object    | 信息模板                                         |
| productDetail         | Object    | 商品详细信息                                     |
| browseCard            | Object    | 卡片信息内容                                     |
| msgTimer              | Object    | 信息发送定时器                                   |
| currentEasy           | Integer   | 当前快捷回复树状图 Id                            |
| pageIndex             | Integer   | 当前聊天内容页数                                 |
| pageSize              | Integer   | 当前聊天内容一页的条数                           |
| current_state         | Integer   | 当前工具栏 Id                                    |
| temporaryReceptNumber | Integer   | 临时客服接待人数，用于记录修改接待人数钱的原人数 |
| sendInfo              | String    | 发送的聊天内容                                   |
| abutmentUrl           | String    | 工具栏对接页面的 url                             |
| temporaryUserName     | String    | 临时客服接待人数，用于记录修改昵称前的原昵称     |
| customerImg           | String    | 用户头像                                         |
| serviceImg            | String    | 客服头像                                         |
| messageTip            | String    | 提示内容                                         |
| noCode                | Timestamp | 每次进入客服功能的时间戳                         |
| isSelectSession       | Boolean   | 是否选中会话                                     |
| expressionShow        | Boolean   | 是否显示表情                                     |
| changeReceptNumber    | Boolean   | 是否修改接待用户数量                             |
| changeUserName        | Boolean   | 是否修改客服昵称                                 |
| showProductDetail     | Boolean   | 是否展示商品详情                                 |
| showLoginBtn          | Boolean   | 是否显示在线状态按钮                             |
| loginAgain            | Boolean   | 是否重新登录                                     |
| currentHasPeople      | Boolean   | 是否有用户接入                                   |
| onlineShow            | Boolean   | 是否显示在线列表                                 |
| offlineShow           | Boolean   | 是否显示离线列表                                 |
| lastSession           | Boolean   | 是否为全部会话内容                               |
| sendState             | Boolean   | 是否可以发送                                     |
| allowSession          | Boolean   | 是否允许会话                                     |
| fastReplay            | Array     | 快捷回复内容                                     |
| conversition          | Array     | 当前用户会话内容                                 |
| currentSessionPeople  | Array     | 当前在线会话列表                                 |
| offLineSessionPeople  | Array     | 离线会话列表                                     |
| expressions           | Array     | 表情列表                                         |
| serviceTool           | Array     | 客服工具栏列表                                   |
| fastReplay            | Array     | 快捷回复内容                                     |

## infoTemplate 数据

| 字段          | 类型      | 描述                                                                 |
| ------------- | --------- | -------------------------------------------------------------------- |
| SendId        | Integer   | 发送者 Id                                                            |
| ReviceId      | Integer   | 接收者 Id                                                            |
| Content       | String    | 发送内容                                                             |
| Identity      | Integer   | 发送者身份：0 机器人，1 客服员，2.会员                               |
| Type          | Integer   | 信息类型 ：0 文本，1 图片，2 表情，3 商品卡片/订单卡片，4 机器人回复 |
| State         | Integer   | 信息发送状态                                                         |
| NoCode        | Timestamp | 发送者时间戳                                                         |
| OutTradeNo    | String    | 发送者 socketId                                                      |
| CreateDateUtc | String    | 发送信息时间                                                         |
| Title         | String    | 推送卡片-标题                                                        |
| Description   | String    | 推送卡片-描述                                                        |
| Label         | String    | 推送卡片-标签                                                        |
| Thumbnail     | String    | 推送卡片-图片                                                        |
| NoSend        | Boolean   | 卡片标题                                                             |

## sender、revicer 数据

| 字段        | 类型    | 描述          |
| ----------- | ------- | ------------- |
| id          | Integer | Id            |
| isService   | Integer | 是否客服      |
| name        | String  | 名称          |
| onlineState | Boolean | 在线状态      |
| outTradeNo  | Integer | 用户 socketId |
| source      | Integer | 来源          |
| mobile      | String  | 手机号        |
| nickName    | String  | 昵称          |
| cardNo      | String  | 卡号          |
| receptNum   | Integer | 接待用户数量  |
