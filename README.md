<h3 align="center">UniChat客服聊天 小程序版</h3>

<h4 align="center">网页、小程序</h4>

<div align="center">

[![star](https://gitee.com/service-chat/uniChat/badge/star.svg)](https://gitee.com/service-chat/uniChat)  [![fork](https://gitee.com/service-chat/uniChat/badge/fork.svg)](https://gitee.com/service-chat/uniChat)  [![](https://img.shields.io/badge/QQ群-149091283-red)](https://jq.qq.com/?_wv=1027&k=XivFMfBQ)



```shell
无偿开源！你们的Star是我的动力！
```

------------------------------------------------------------------------
</div>

## 客服功能一览

- 机器人智能聊天
- 客服手动在线离线
- 用户主动向客服发送信息（信息包括文本、表情包）
- 客服选择会话成员，并且主动向用户发送信息（信息包括文本、表情包）
- 用户/客服接收到对方发送的信息
- 客服主动关闭用户会话，离线列表显示离线用户，用户端提示客服主动关闭会话，本次会话结束
- 客服手动离线，清除所有会话列表，用户端提示客服已离线，本次会话结束
- 客服刷新或关闭页面下线，清除所有会话列表，用户端提示客服已离线，本次会话结束
- 用户刷新页面或关闭页面，客服端提示用户已下线，本次会话结束
- 客服切换右边工具栏，选择快捷回复，可选中快捷回复信息以此快速回复内容
- 发送信息，如果服务器中断，信息状态为 0（未发送出），若 20 秒服务器仍为断开，信息状态改成-1（发送失败），若 20 秒内服务器恢复，信息状态改成 1（发送成功）
- 在用户端修改 openImitateProduct 字段为 true，可发送商品卡片
- 客服接收用户发送的商品卡片，并且查看详情
- 完成图片发送，若图片过大时进行图片压缩，图片超大时不允许发送
- 完成图片接收，查看
- 用户多台设备在线时，强制另一台设备下线
- 客服多台设备在线时，强制旧客服端下线，并且中断会员的会话

## 演示

### 客户端

![客户端](https://img-blog.csdnimg.cn/20210525091120198.gif#pic_center)

### 客服端

![客服端](https://img-blog.csdnimg.cn/20210525091249849.gif#pic_center)

如果对您对此项目有兴趣，可以点 “Star” 支持一下 谢谢！

如果有任何的疑惑或建议，请在评论中提出，欢迎评论！

服务器(server 文件)运行：

```javascript
npm install
node app.js
```

前端客服端(service 文件)运行：

```javascript
npm install
npm  run serve
在路由后面补全 ?sendId=1 ，可切换不同的客服身份
```

前端客户端(customer 文件)：

```javascript
使用Hbuilder X 工具导入文件，并且运行编译
```

## QQ群
- 群号：149091283 （进群备注）