const TOOLBOX = [{
		name: "照片",
		image: require("../../static/img/service/selectPhone.png"),
	},
	{
		name: "服务评价",
		image: require("../../static/img/service/apraise.png"),
	},
];

const ROBOTTOOLBOX = [{
	name: "留言",
	image: require("../../static/img/service/apraise.png"),
}]

const EXPRESSIONS = [{
		title: "[呵呵]",
		image: require("../../static/img/Expression/smilea_thumb.gif"),
	},
	{
		title: "[嘻嘻]",
		image: require("../../static/img/Expression/tootha_thumb.gif"),
	},
	{
		title: "[哈哈]",
		image: require("../../static/img/Expression/laugh.gif"),
	},
	{
		title: "[可爱]",
		image: require("../../static/img/Expression/tza_thumb.gif"),
	},
	{
		title: "[可怜]",
		image: require("../../static/img/Expression/kl_thumb.gif"),
	},
	{
		title: "[挖鼻屎]",
		image: require("../../static/img/Expression/kbsa_thumb.gif"),
	},
	{
		title: "[吃惊]",
		image: require("../../static/img/Expression/cj_thumb.gif"),
	},
	{
		title: "[害羞]",
		image: require("../../static/img/Expression/shamea_thumb.gif"),
	},
	{
		title: "[挤眼]",
		image: require("../../static/img/Expression/zy_thumb.gif"),
	},
	{
		title: "[闭嘴]",
		image: require("../../static/img/Expression/bz_thumb.gif"),
	},
	{
		title: "[鄙视]",
		image: require("../../static/img/Expression/bs2_thumb.gif"),
	},
	{
		title: "[爱你]",
		image: require("../../static/img/Expression/lovea_thumb.gif"),
	},
	{
		title: "[泪]",
		image: require("../../static/img/Expression/sada_thumb.gif"),
	},
	{
		title: "[偷笑]",
		image: require("../../static/img/Expression/heia_thumb.gif"),
	},
	{
		title: "[亲亲]",
		image: require("../../static/img/Expression/qq_thumb.gif"),
	},
	{
		title: "[生病]",
		image: require("../../static/img/Expression/sb_thumb.gif"),
	},
	{
		title: "[太开心]",
		image: require("../../static/img/Expression/mb_thumb.gif"),
	},
	{
		title: "[懒得理你]",
		image: require("../../static/img/Expression/ldln_thumb.gif"),
	},
	{
		title: "[右哼哼]",
		image: require("../../static/img/Expression/yhh_thumb.gif"),
	},
	{
		title: "[左哼哼]",
		image: require("../../static/img/Expression/zhh_thumb.gif"),
	},
	{
		title: "[嘘]",
		image: require("../../static/img/Expression/x_thumb.gif"),
	},
	{
		title: "[衰]",
		image: require("../../static/img/Expression/cry.gif"),
	},
	{
		title: "[委屈]",
		image: require("../../static/img/Expression/wq_thumb.gif"),
	},
	{
		title: "[吐]",
		image: require("../../static/img/Expression/t_thumb.gif"),
	},
	{
		title: "[打哈气]",
		image: require("../../static/img/Expression/k_thumb.gif"),
	},
	{
		title: "[抱抱]",
		image: require("../../static/img/Expression/bba_thumb.gif"),
	},
	{
		title: "[怒]",
		image: require("../../static/img/Expression/angrya_thumb.gif"),
	},
	{
		title: "[疑问]",
		image: require("../../static/img/Expression/yw_thumb.gif"),
	},
	{
		title: "[馋嘴]",
		image: require("../../static/img/Expression/cza_thumb.gif"),
	},
	{
		title: "[拜拜]",
		image: require("../../static/img/Expression/88_thumb.gif"),
	},
	{
		title: "[思考]",
		image: require("../../static/img/Expression/sk_thumb.gif"),
	},
	{
		title: "[汗]",
		image: require("../../static/img/Expression/sweata_thumb.gif"),
	},
	{
		title: "[困]",
		image: require("../../static/img/Expression/sleepya_thumb.gif"),
	},
	{
		title: "[睡觉]",
		image: require("../../static/img/Expression/sleepa_thumb.gif"),
	},
	{
		title: "[钱]",
		image: require("../../static/img/Expression/money_thumb.gif"),
	},
	{
		title: "[失望]",
		image: require("../../static/img/Expression/sw_thumb.gif"),
	},
	{
		title: "[酷]",
		image: require("../../static/img/Expression/cool_thumb.gif"),
	},
	{
		title: "[花心]",
		image: require("../../static/img/Expression/hsa_thumb.gif"),
	},
	{
		title: "[哼]",
		image: require("../../static/img/Expression/hatea_thumb.gif"),
	},
	{
		title: "[鼓掌]",
		image: require("../../static/img/Expression/gza_thumb.gif"),
	},
	{
		title: "[晕]",
		image: require("../../static/img/Expression/dizzya_thumb.gif"),
	},
	{
		title: "[悲伤]",
		image: require("../../static/img/Expression/bs_thumb.gif"),
	},
]

const STARTLIST = [{
		offstart: require("../../static/img/service/offstart.png"),
		onstart: require("../../static/img/service/onstart.png"),
		state: false,
		level: 1,
	},
	{
		offstart: require("../../static/img/service/offstart.png"),
		onstart: require("../../static/img/service/onstart.png"),
		state: false,
		level: 2,
	},
	{
		offstart: require("../../static/img/service/offstart.png"),
		onstart: require("../../static/img/service/onstart.png"),
		state: false,
		level: 3,
	},
	{
		offstart: require("../../static/img/service/offstart.png"),
		onstart: require("../../static/img/service/onstart.png"),
		state: false,
		level: 4,
	},
	{
		offstart: require("../../static/img/service/offstart.png"),
		onstart: require("../../static/img/service/onstart.png"),
		state: false,
		level: 5,
	},
];

const SOLVESTATE = [{
		name: "已解决",
		state: false,
		value: 1,
	},
	{
		name: "未解决",
		state: false,
		value: 0,
	},
]

const SERVICETOOL = [{
		id: 1,
		text: "客户信息",
		state: true,
	},
	{
		id: 2,
		text: "快捷回复",
		state: false,
	},
	{
		id: 3,
		text: "对接页面",
		state: false,
	},
]

module.exports = {
	TOOLBOX,
	ROBOTTOOLBOX,
	EXPRESSIONS,
	STARTLIST,
	SOLVESTATE,
	SERVICETOOL
}
